var React = require('React');
require('./css/addItem.css');

var AddItem = React.createClass({
  render: function(){
    return(
      <form id="add-todo" onSubmit={this.handleSubmit}>
        <input type="text" required ref={(input) => this.textInput = input}/>
        <input type="submit" value="Hit me" />
      </form>
    );
  },

  //Custom functions
  handleSubmit: function(e){
    e.preventDefault();
    this.props.onAdd(this.textInput.value)
    this.textInput.value="";
  }
});

module.exports = AddItem;
